	return new Promise((resolve, reject) => {

		var currentDate = moment().format();
		var dates = [];

		for (var i = 1; i < days + 1; i++) {
			dates.push(moment().subtract(i, "days"));
		}

		resolve(dates);
	})

	.then(dates => {

		var dateObjects = [];

		dates.forEach(date => dateObjects.push({
			date  : date,
			meals : {}
		}))

		return dateObjects;
	})

	.then(dateObjects => {

		return Promise.try(=> {
			var query = `
				SELECT FR.id, FR.rating, FR.body, FR.googleID, FR.date, FR.meal
				FROM food AS FR
				ORDER BY FR.date DESC
			`
			
			return connection.queryAsync(query);
		}).then(rows => {
			return {dateObjects, rows};
		})

		//return connection.query(query, function(err, rows) {
		//	if (err) Promise.reject(err);
		//	else {
		//		console.log("returning");
		//		Promise.resolve({ dateObjects : dateObjects, rows : rows })
		//	}
		//});

	})

	.then(container => {

		console.log("CONTAINER: " + container)

		var dateObjects = container.dateObjects;
		var rows        = container.rows;

		console.log("HERE: " + dateObjects);

		// Cycle through each date object in that range
		dateObjects.forEach(object => {

			// Get the reviews that match the date of the current date object
			// and then fill that date object with those reviews.

			var reviews = rows.filter(date => {
				return date.toDateString() === object.date.toDateString()
			})

			// Filter the reviews into their respective meal arrays

			var breakfast = reviews.filter(r => { return r.meal === "BREAKFAST" });
			var brunch    = reviews.filter(r => { return r.meal === "BRUNCH" });
			var lunch     = reviews.filter(r => { return r.meal === "LUNCH" });
			var dinner    = reviews.filter(r => { return r.meal === "DINNER" });

			// Populate the dataObject with their meal arrays

			object.breakfast = breakfast;
			object.brunch    = brunch;
			object.lunch     = lunch;
			object.dinner    = dinner;

		})

		console.log("DATE OBJECTS: \n\n" + JSON.stringify(dateObjects, 2, 2));

	})